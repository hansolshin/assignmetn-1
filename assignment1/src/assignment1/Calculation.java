package assignment1;

import java.util.Scanner;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.math.RoundingMode;

public class Calculation 
{
	public static void main(String[] args) 
	{
		int radiusin, depthft, answer, familymember;
		float radiusft;
		
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter the number of family members in your household:"); // gets number of family members
		familymember = keyboard.nextInt();
		System.out.println("Enter the radius of the well in inches:"); // radius of well input
		radiusin = keyboard.nextInt();
		System.out.println("Enter the depth of the well in feet:"); // height of well input
		depthft = keyboard.nextInt();
		radiusft = (float) (radiusin * 0.8333333);
		keyboard.close();
		System.out.println("From the given values,");
		System.out.println("the water cased in the well (in gal):");
		
		answer = (int)(radiusft * radiusft * 3.14 * depthft * 7.48); // calculates how much gallon is stored
		NumberFormat df = DecimalFormat.getInstance();
		df.setMinimumFractionDigits(2);
		df.setMaximumFractionDigits(4);
		df.setRoundingMode(RoundingMode.DOWN);
		System.out.println(df.format(answer) + " gallons of water cased in the well"); // decimal place rounded to two
		
		if (answer > 62.5 * familymember)
		{
			System.out.println("You have enough water cased");
		}
			else 
				System.out.println("you need more water");
	}
	}